﻿using CandyShop.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CandyShop.ViewModels
{
    public class PyjamaViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        
        public decimal Price { get; set; }
        public IFormFile ImageUrl { get; set; }
        public IFormFile ImageThumbnailUrl { get; set; }
        public bool IsOnSale { get; set; }
        public bool IsInStock { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }

    }
}
