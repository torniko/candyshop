﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CandyShop.Migrations
{
    public partial class atwork : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShopppingCartiTems_Candies_CandyId",
                table: "ShopppingCartiTems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ShopppingCartiTems",
                table: "ShopppingCartiTems");

            migrationBuilder.RenameTable(
                name: "ShopppingCartiTems",
                newName: "ShopppingCartItems");

            migrationBuilder.RenameColumn(
                name: "ShoppingCartID",
                table: "ShopppingCartItems",
                newName: "ShoppingCartId");

            migrationBuilder.RenameColumn(
                name: "ShoppingCartItemID",
                table: "ShopppingCartItems",
                newName: "ShoppingCartItemId");

            migrationBuilder.RenameIndex(
                name: "IX_ShopppingCartiTems_CandyId",
                table: "ShopppingCartItems",
                newName: "IX_ShopppingCartItems_CandyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ShopppingCartItems",
                table: "ShopppingCartItems",
                column: "ShoppingCartItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShopppingCartItems_Candies_CandyId",
                table: "ShopppingCartItems",
                column: "CandyId",
                principalTable: "Candies",
                principalColumn: "CandyId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShopppingCartItems_Candies_CandyId",
                table: "ShopppingCartItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ShopppingCartItems",
                table: "ShopppingCartItems");

            migrationBuilder.RenameTable(
                name: "ShopppingCartItems",
                newName: "ShopppingCartiTems");

            migrationBuilder.RenameColumn(
                name: "ShoppingCartId",
                table: "ShopppingCartiTems",
                newName: "ShoppingCartID");

            migrationBuilder.RenameColumn(
                name: "ShoppingCartItemId",
                table: "ShopppingCartiTems",
                newName: "ShoppingCartItemID");

            migrationBuilder.RenameIndex(
                name: "IX_ShopppingCartItems_CandyId",
                table: "ShopppingCartiTems",
                newName: "IX_ShopppingCartiTems_CandyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ShopppingCartiTems",
                table: "ShopppingCartiTems",
                column: "ShoppingCartItemID");

            migrationBuilder.AddForeignKey(
                name: "FK_ShopppingCartiTems_Candies_CandyId",
                table: "ShopppingCartiTems",
                column: "CandyId",
                principalTable: "Candies",
                principalColumn: "CandyId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
