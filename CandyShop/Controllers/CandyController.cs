﻿using CandyShop.Models;
using CandyShop.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CandyShop.Controllers
{
    public class CandyController : Controller
    {
        private readonly ICandyRepository _candyRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly AppDbContext _appDbContext;
        private readonly IWebHostEnvironment WebHostEnvironment;

        public CandyController(ICandyRepository candyRepository, ICategoryRepository categoryRepository, AppDbContext appDbContext, IWebHostEnvironment webHostEnvironment)
        {
            _candyRepository = candyRepository;
            _categoryRepository = categoryRepository;
            _appDbContext = appDbContext;
            WebHostEnvironment = webHostEnvironment;
        }

        public ViewResult List(PyjamaViewModel obj, string category)
        {
            IEnumerable<Candy> candies;
            string currentCategory;


            if (string.IsNullOrEmpty(category))
            {
                candies = _candyRepository.GetAllCandy.OrderBy(c => c.CandyId);
                currentCategory = "All Candy";
            } 
            else
            {
                candies = _candyRepository.GetAllCandy.Where(c => c.Category.CategoryName == category);
                currentCategory = _categoryRepository.GetAllCategories.FirstOrDefault(c => c.CategoryName == category)?.CategoryName;
            }

            return View(new CandyListViewModel
            {
                Candies = candies,
                CurrentCategory = currentCategory
            });
        }
        public ViewResult SerachPhraseResult(PyjamaViewModel obj, string category, string SearchResult)
        {
            IEnumerable<Candy> candies;
            string currentCategory;


            if (string.IsNullOrEmpty(category))
            {
                candies = _candyRepository.GetAllCandy.OrderBy(c => c.CandyId);
                currentCategory = "All Candy";
            }
            else
            {
                candies = _candyRepository.GetAllCandy.Where(c => c.Name.Contains(SearchResult));
                currentCategory = _categoryRepository.GetAllCategories.FirstOrDefault(c => c.CategoryName == category)?.CategoryName;
            }

            return View(new CandyListViewModel
            {
                Candies = candies,
                CurrentCategory = currentCategory
            });
        }


            public ViewResult Index(PyjamaViewModel obj, string category)
        {
            IEnumerable<Candy> candies;
            string currentCategory;


            if (string.IsNullOrEmpty(category))
            {
                candies = _candyRepository.GetAllCandy.OrderBy(c => c.CandyId);
                currentCategory = "All Candy";
            }
            else
            {
                candies = _candyRepository.GetAllCandy.Where(c => c.Category.CategoryName == category);
                currentCategory = _categoryRepository.GetAllCategories.FirstOrDefault(c => c.CategoryName == category)?.CategoryName;
            }

            return View(new CandyListViewModel
            {
                Candies = candies,
                CurrentCategory = currentCategory
            });
        }



        public IActionResult Details(int id)
        {
            var candy = _candyRepository.GetCandyById(id);
            if (candy == null)
            {
                return NotFound();
            }
            return View(candy);
        }

        public IActionResult Create()
        {


            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PyjamaViewModel obj)
        {
            string stringFilename = FileUpload(obj);
            string stringFilename2 = FileUpload2(obj);
            var pyjama = new Candy
            {
                Name = obj.Name,
                Price = obj.Price,
                ImageThumbnailUrl = stringFilename,
                Description = obj.Description,
                ImageUrl = stringFilename2,
                CategoryId = obj.CategoryId,
                Category = obj.Category,
                IsInStock =obj.IsInStock,
                IsOnSale = obj.IsOnSale
            };

            _appDbContext.Candies.Add(pyjama);
            _appDbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        private string FileUpload(PyjamaViewModel obj)
        {
            string fileName = null;
            if (obj.ImageThumbnailUrl!= null)
            {
                string uploadDir = Path.Combine(WebHostEnvironment.WebRootPath, "Images/thumbnails");
                fileName = Guid.NewGuid().ToString() + "-" + obj.ImageThumbnailUrl.FileName;
                string filepath = Path.Combine(uploadDir, fileName);
                using (var filestream = new FileStream(filepath, FileMode.Create))
                {
                    obj.ImageThumbnailUrl.CopyTo(filestream);
                }
            }
            return "\\Images\\thumbnails\\"+fileName;
        }
        private string FileUpload2(PyjamaViewModel obj)
        {
            string fileName = null;
            if (obj.ImageUrl != null)
            {
                string uploadDir = Path.Combine(WebHostEnvironment.WebRootPath, "Images/");
                fileName = Guid.NewGuid().ToString() + "-" + obj.ImageUrl.FileName;
                string filepath = Path.Combine(uploadDir, fileName);
                using (var filestream = new FileStream(filepath, FileMode.Create))
                {
                    obj.ImageUrl.CopyTo(filestream);
                }
            }
            return "\\Images\\" + fileName;
        }




        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pyjama = await _appDbContext.Candies
                .FirstOrDefaultAsync(m => m.CandyId == id);
            if (pyjama == null)
            {
                return NotFound();
            }

            return View(pyjama);
        }

        // POST: Pyjamas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var pyjama = await _appDbContext.Candies.FindAsync(id);
            _appDbContext.Candies.Remove(pyjama);
            await _appDbContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PyjamaExists(int id)
        {
            return _appDbContext.Candies.Any(e => e.CandyId == id);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pyjama = await _appDbContext.Candies.FindAsync(id);
            if (pyjama == null)
            {
                return NotFound();
            }
            return View(pyjama);
        }

        // POST: Pyjamas/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Price,ProfileImage")] Candy pyjama)
        {
            if (id != pyjama.CandyId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _appDbContext.Update(pyjama);
                    await _appDbContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PyjamaExists(pyjama.CandyId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(pyjama);
        }

    }
}
